import argparse
import json

try:
    from plistlib import load
except ImportError:
    from plistlib import readPlist as load


class MonarchTheme(object):
    property_map = {
        'background': 'background',
        'foreground': 'foreground',
        'fontStyle': 'fontStyle',
        'token': 'scope'
    }

    color_map = {
        'editor.background': 'background',
        'editor.foreground': 'foreground',
        'editor.lineHighlightBackground': 'lineHighlight',
        'editorCursor.foreground': 'caret',
        'selection.background': 'selection',
        # XXX - This might be more than indent guides
        'editorIndentGuide.background': 'invisibles',
    }

    def __init__(self, settings, rules, base):
        self.theme = {
            'base': base,
            'inherit': True,
            'rules': [],
            'colors': {}
        }

        self.make_colors(settings)

        for rule in rules:
            scopes = rule.get('scope').split(' ')
            if len(scopes) > 1:
                # Create 1 token def per scope
                for scope in scopes:
                    self.make_rule(scope, rule)
            else:
                self.make_rule(scopes[0], rule)

        # NOTE - Many things fallback to "identifier" by default
        # it would be nearly impossible to catch all patterns in all
        # langs, so just apply the base font style to this scope
        self.theme.get('rules').append(
            {'token': 'identifier',
             'foreground': '202020' if self.theme.get('base') == 'vs' else 'FAFBFC'})

    def make_rule(self, scope, rule):
        settings = rule.get('settings')
        res = {'token': scope}

        for a, b in self.property_map.items():
            if b in settings:
                # XXX - Colors cant be null, this probably affects the generated theme...
                if b == 'fontStyle' or settings.get(b):
                    res[a] = settings.get(b).lstrip('#')
        self.theme.get('rules').append(res)

    def make_colors(self, settings):
        for a, b in self.color_map.items():
            if settings.get(b):
                self.theme.get('colors')[a] = settings.get(b)

    def to_json(self, pretty=False):
        # FIXME - This should be renamed to something like `render` when refactoring for re-use
        kw = {}
        if pretty:
            kw['indent'] = 2
        return json.dumps(self.theme, **kw)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Given a textmate theme, convert to various output formats')
    parser.add_argument('filename')
    parser.add_argument('vsbase')  # Lookup vs-base here instead? package.json?
    args = parser.parse_args()

    with open(args.filename, 'rb') as fp:
        theme = load(fp)

    settings = theme.get('settings').pop(0).get('settings')
    rules = theme.get('settings')

    print(MonarchTheme(settings, rules, args.vsbase).to_json(pretty=True))
